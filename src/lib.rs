#![deny(missing_docs)]

/*!
Derive macros for the token parser.
**/

use quote::quote;
use syn::{
    parse_macro_input, parse_quote, Data, DeriveInput, Fields, GenericParam, Generics,
    TypeParamBound,
};

#[proc_macro_derive(Parsable)]
/// Derive the `Parsable` trait by parsing all unnamed fields in order using the `Parsable` trait.
pub fn derive_default_parsable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    derive_parsable(input, false)
}

#[proc_macro_derive(SymbolParsable)]
/// Derive the `Parsable` trait by parsing all unnamed fields using the `FromStr` trait.
pub fn derive_symbol_parsable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    derive_parsable(input, true)
}

fn add_trait_bounds(mut generics: Generics, bound: TypeParamBound) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(bound.clone());
        }
    }
    generics
}

fn derive_parsable(input: proc_macro::TokenStream, symbol: bool) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let trait_bound = if symbol {
        parse_quote!(std::str::FromStr)
    } else {
        parse_quote!(token_parser::Parsable)
    };

    let generics = add_trait_bounds(input.generics.clone(), trait_bound);
    let (_, ty_generics, where_clause) = generics.split_for_impl();

    let mut generics = input.generics;
    generics.params.push(parse_quote!(C: token_parser::Context));
    let (impl_generics, _, _) = generics.split_for_impl();

    if let Data::Struct(data) = input.data {
        if let Fields::Unnamed(fields) = data.fields {
            let expression = if symbol {
                quote! {
                    if let Ok(result) = name.parse() {
                        result
                    } else {
                        return Err(token_parser::Error::StringParsing)
                    }
                }
            } else {
                quote!(parser.parse_next(context)?)
            };

            let expressions = fields.unnamed.into_iter().map(|_| &expression);

            let method = if symbol {
                quote! {
                    fn parse_symbol(name: String, context: &C) -> token_parser::Result<Self>
                }
            } else {
                quote! {
                    fn parse_list(parser: &mut token_parser::Parser, context: &C) -> token_parser::Result<Self>
                }
            };

            let expanded = quote! {
                impl #impl_generics token_parser::Parsable<C> for #name #ty_generics #where_clause {
                    #method
                    {
                        Ok(#name(#( #expressions, )*))
                    }
                }
            };

            proc_macro::TokenStream::from(expanded)
        } else {
            panic!("Deriving parser is only available for tuple structs")
        }
    } else {
        panic!("Deriving parser is only available for structs")
    }
}
